import json

from test01.json_tool import json_serial


class JsonDataResult:
    success: bool = False
    errorCode: int = 0
    message: str = ''
    data: dict = None

    def keys(self):
        return ('success', 'errorCode', 'message', 'data')

    def __getitem__(self, item):
        return getattr(self, item)

    def to_json(self):
        return json.dumps(dict(self), default=json_serial, sort_keys=True, indent=4, ensure_ascii=False)
