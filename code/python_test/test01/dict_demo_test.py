import json
import unittest
from datetime import datetime

from test01.json_data_result import JsonDataResult
from test01.json_tool import json_serial
from test01.obj_dict_tool import objDictTool


class User:
    is_admin = False
    desc = ''

    def __init__(self, name) -> None:
        self.name = name
        self.age = 1


class Order:
    order_no: str = ''
    desc = ''

    def __init__(self, name) -> None:
        self.name = name
        self.age = 1


class Product:
    price: int = 10
    __author: str = '沐雨云楼'
    desc: str = ''

    def __init__(self, name: str) -> None:
        self.name = name

    def keys(self):
        '''当对实例化对象使用dict(obj)的时候, 会调用这个方法,这里定义了字典的键, 其对应的值将以obj['name']的形式取,
       但是对象是不可以以这种方式取值的, 为了支持这种取值, 可以为类增加一个方法'''
        return ('name', 'price', 'desc')

    def __getitem__(self, item):
        '''内置方法, 当使用obj['name']的形式的时候, 将调用这个方法, 这里返回的结果就是值'''
        return getattr(self, item)


class Comment:
    title: str = ''
    author: str = ''
    desc: str = ''
    create_time: datetime = datetime.now()


class MyTestCase(unittest.TestCase):
    def test_user_by__dict__(self):
        user = User('iworkh')
        user.is_admin = True
        # 只有塞值的时候，才会转化为dic
        # desc没有塞值，dic中没有
        dic = {}
        start0 = datetime.now()
        print(start0)
        for no in range(10000000):
            dic = user.__dict__
        start1 = datetime.now()
        print(start1)
        print(start1 - start0)
        print(dic)
        pass

    def test_product(self):
        product = Product('book')
        start0 = datetime.now()
        print(start0)
        for no in range(10000000):
            dict(product)
        start1 = datetime.now()
        print(start1)
        print(start1 - start0)
        pass

    def test_order_by_attr(self):
        user = User('iworkh')
        dic = {}
        start0 = datetime.now()
        print(start0)
        for no in range(10000000):
            dic = objDictTool.to_dic(user)
        start1 = datetime.now()
        print(start1)
        print(start1 - start0)
        print(dic)
        pass

    def test_dict_to_obj(self):
        product_dic = {'name': 'python', 'price': 10, 'desc': 'python对象和dic转化'}
        product_obj = Product('test')

        start0 = datetime.now()
        print(start0)
        for no in range(10000000):
            objDictTool.to_obj(product_obj, **product_dic)

        start1 = datetime.now()
        print(start1)
        print(start1 - start0)

        print("名称: {}, 价格: {},  描述: {}".format(product_obj.name, product_obj.price, product_obj.desc))
        pass

    def test_json(self):
        # 返回json时，序列化对象要是 python原始类型：
        # dict, list, tuple, str, unicode, int, long, float, True, False, None
        comment = Comment()
        comment.title = 'python工具类-obj转dict'
        comment.author = '沐雨云楼'
        comment.desc = '怎么玩呢？'
        comment.create_time = datetime.now()
        # result = {'success': True, 'data': comment.__dict__}
        # json_str = json.dumps(result, sort_keys=True, indent=4, ensure_ascii=False)

        result = {'success': True, 'data': comment.__dict__}
        # json_str = json.dumps(result, default=lambda obj: obj.__dict__, sort_keys=True, indent=4, ensure_ascii=False)
        json_str = json.dumps(result, default=json_serial, sort_keys=True, indent=4, ensure_ascii=False)
        print(json_str)
        pass

    def test_json_data_result(self):
        comment = Comment()
        comment.title = 'python工具类-obj转dict'
        comment.author = '沐雨云楼'
        comment.desc = '怎么玩呢？'

        result = JsonDataResult()
        result.success = True
        result.data = objDictTool.to_dic(comment)
        print(result.to_json())
        pass

if __name__ == '__main__':
    unittest.main()
