import unittest


class A(object):
    @staticmethod
    def add(a, b):
        return a + b


class MyTestCase(unittest.TestCase):
    def test_staticmethod(self):
        # 类调用
        print(A.add(2, 4))
        # 对象调用
        a = A()
        print(a.add(2, 6))


if __name__ == '__main__':
    unittest.main()
