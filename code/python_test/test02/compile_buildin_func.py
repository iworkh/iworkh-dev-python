import unittest

class MyTestCase(unittest.TestCase):
    def test_compile(self):
        # exec
        source = "for i in range(1,5): print(i)"
        compiled_source = compile(source, '', 'exec')
        exec(compiled_source)
        # eval
        str = "3 * 4 + 5"
        compiled_eval = compile(str, '', 'eval')
        result = eval(compiled_eval)
        print(result)

if __name__ == '__main__':
    unittest.main()
