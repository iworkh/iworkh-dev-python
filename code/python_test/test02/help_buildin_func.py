import unittest


class MyTestCase(unittest.TestCase):
    def test_help(self):
        # help(str)
        # help(list)
        list_data = [1, 2, 3]
        # help(list_data)
        help(list_data.append)


if __name__ == '__main__':
    unittest.main()
