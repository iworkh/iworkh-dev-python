import unittest


class MyTestCase(unittest.TestCase):
    def test_round(self):
        print(round(3))  # 3
        print(round(70.43456))  # 70
        print(round(70.50456))  #  71
        print(round(70.23456, 3))  # 70.235
        print(round(-100.000056, 3))  # -100.0


if __name__ == '__main__':
    unittest.main()
