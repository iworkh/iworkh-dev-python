import unittest

class MyTestCase(unittest.TestCase):
    def test_float(self):
        print(float(1))  # 1.0
        print(float(-123.6))  # -123.6
        print(float('123'))  # 123.0

    def test_int(self):
        print(int())  # 默认0
        print(int(2))  # 2
        print(int(2.5))  # 2
        print(int('1',2))  # 1
        print(int('10',8))  # 8
        print(int('10',10))  # 10
        print(int('10',16))  # 16
        print(int('0x10',16))  # 16

    def test_chr(self):
        print(chr(0x30)) # 0
        print(chr(97)) # a

    def test_bin(self):
        print(bin(97)) # 0b1100001
        print(bin(88)) # 0b1011000

    def test_oct(self):
        print(oct(97)) # 0o141
        print(oct(88)) # 0o130

    def test_hex(self):
        print(hex(97)) # 0x61
        print(hex(88)) # 0x58

    def test_bool(self):
        print(bool()) # False
        print(bool(True)) # True
        print(bool(0)) # False
        print(bool(88)) # True
        print(issubclass(bool, int)) # True: bool 是 int 子类

if __name__ == '__main__':
    unittest.main()
