import unittest


class MyTestCase(unittest.TestCase):
    def test_formart(self):
        print("{} - {}".format('iworkh', '沐雨云楼'))  # iworkh - 沐雨云楼
        print("{1} - {0}".format('iworkh', '沐雨云楼'))  # 沐雨云楼 - iworkh
        print("{company} - {name}".format(company='iworkh', name='沐雨云楼'))  # iworkh - 沐雨云楼
        # 字典
        data_dic = {'company': 'iworkh', 'name': '沐雨云楼'}
        print("网站：{company} - 名称：{name}".format(**data_dic))  # 网站：iworkh - 名称：沐雨云楼
        # 列表: 说明 0[1]中的0代表是data_list，1表示list里第一元素
        data_list1 = ['iworkh', '沐雨云楼']
        data_list2 = ['test', 'yuxl']
        print("网站：{0[0]} - 名称：{1[1]}".format(data_list1, data_list2))  # 网站：iworkh - 名称：沐雨云楼
        # 格式化
        print("{:.2f}".format(3.1415926))
        # {}的转义
        print("{{}}是表示转移的,{:.2%}".format(0.26))


if __name__ == '__main__':
    unittest.main()
