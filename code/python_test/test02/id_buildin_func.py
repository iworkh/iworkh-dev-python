import unittest


class User:
    name: str
    age: int


class MyTestCase(unittest.TestCase):
    def test_id(self):
        print(id(1))
        # 字符串
        print(id("2"))
        # tuple
        print(id((1, False, '3')))
        # list
        print(id([1, False, '3']))
        # dict
        print(id({'name': 'iworkh', 'age': 20}))
        # 对象
        user = User()
        user.name = 'iworkh'
        user.age = 120
        print(id(user))


if __name__ == '__main__':
    unittest.main()
