import unittest


class MyTestCase(unittest.TestCase):
    def test_complex(self):
        a = complex(3, 4)
        b = complex(1)
        c = complex('3')
        d = complex("3+4j")
        print(a) # (3+4j)
        print(b) # (1+0j)
        print(c) # (3+0j)
        print(d) # (3+4j)


if __name__ == '__main__':
    unittest.main()
