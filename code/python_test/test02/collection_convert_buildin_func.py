import unittest


class MyTestCase(unittest.TestCase):
    def test_str(self):
        dict = {'company': 'iworkh', 'name': '沐雨云楼'}
        print(str(dict))  # {'company': 'iworkh', 'name': '沐雨云楼'}
        list = ['iworkh', '沐雨云楼']
        print(str(list))  # ['iworkh', '沐雨云楼']

    def test_iter(self):
        list = ['iworkh', '沐雨云楼']
        print(iter(list))  # <list_iterator object at 0x000002D9B9E35D48>
        for item in iter(list):
            print(item)

        dict = {'company': 'iworkh', 'name': '沐雨云楼'}
        print(iter(dict))  # <dict_keyiterator object at 0x0000020ACFCE07C8>
        for item_key in iter(dict):
            print("{}-{}".format(item_key, dict[item_key]))

    def test_dict(self):
        print(dict())  # 空dict {}
        print(dict(name='沐雨云楼', age=20, sex='男'))  # {'name': '沐雨云楼', 'age': 20, 'sex': '男'}
        list_zip = zip(['name', 'age', 'sex'], ['沐雨云楼', 20, '男'])
        print(dict(list_zip))  # {'name': '沐雨云楼', 'age': 20, 'sex': '男'}
        tuple_list = [('name', '沐雨云楼'), ('age', 20), ('sex', '男')]
        print(dict(tuple_list))  # {'name': '沐雨云楼', 'age': 20, 'sex': '男'}

    def test_list(self):
        tuple = ('iworkh', 20, '男')
        print(list(tuple))  # ['iworkh', 20, '男']
        print(list('iowrkh 沐雨云楼'))  # ['i', 'o', 'w', 'r', 'k', 'h', ' ', '沐', '雨', '云', '楼']

    def test_tuple(self):
        list = ['iworkh', '沐雨云楼']
        print(tuple(list))  # ('iworkh', '沐雨云楼')

    def test_set(self):
        set1 = {1, 2, 3}
        list = [3, 4, 5]
        set2 = set(list)
        print(set1 & set2)  # 交集：{3}
        print(set1 | set2)  # 并集：{1, 2, 3, 4, 5}
        print(set1 - set2)  # 差集：{1, 2}

    def test_frozenset(self):
        print(frozenset(range(5)))  # frozenset({0, 1, 2, 3, 4})
        list = ['iworkh', '沐雨云楼']
        print(frozenset(list))  # frozenset({'沐雨云楼', 'iworkh'})

    def test_sorted(self):
        list = [10, 1, 4, 3, 4]
        print(list)
        print(sorted(list)) # 返回的新list
        print(list)
        print(list.sort()) # 在原来的list上操作
        print(list)


if __name__ == '__main__':
    unittest.main()
