import unittest


class People:
    sex: str


class User(People):
    name: str
    age: int


class MyTestCase(unittest.TestCase):
    def test_isinstance(self):
        a = 10
        print(isinstance(a, int))  # True
        print(isinstance(a, float))  # False
        print(isinstance(a, (int, float, str)))  # True 满足一个即可
        # 对象
        print(isinstance(User(), User))  # True
        print(type(User()) == User)  # True
        print(isinstance(User(), People))  # True  isinstance考虑继承
        print(type(User()) == People)  # False type不考虑继承


pass

if __name__ == '__main__':
    unittest.main()
