import math
import unittest


class MyTestCase(unittest.TestCase):
    def test_divmod(self):
        print(pow(3, 0))  # 1 原生
        print(pow(3, 2))  # 9 原生
        print(math.pow(3, 2))  # 9.0 math的


if __name__ == '__main__':
    unittest.main()
