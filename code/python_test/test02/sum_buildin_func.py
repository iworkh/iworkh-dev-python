import unittest


class MyTestCase(unittest.TestCase):
    def test_sum(self):
        print(sum([1,2,3,4]))  # 10 列表
        print(sum((1,2,3,4)))  # 10 元组
        print(sum((1,2,3,4),10))  # 20

if __name__ == '__main__':
    unittest.main()
