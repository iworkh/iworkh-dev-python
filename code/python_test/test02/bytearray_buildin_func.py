import unittest


class MyTestCase(unittest.TestCase):
    def test_bytearray(self):
        # str
        print(bytearray('abcde', 'utf-8'))

        # list
        list_data = [1, 2, 3, 4]
        print(bytearray(list_data))

if __name__ == '__main__':
            unittest.main()
