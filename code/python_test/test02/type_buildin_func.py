import unittest


class People:
    sex: str


class User(People):
    name: str
    age: int


class LL(object):
    a = 1
    pass


class MyTestCase(unittest.TestCase):
    def test_type(self):
        # int
        print(type(10))  # <class 'int'>
        # list
        print(type([1, 2, 3]))  # <class 'list'>

        # 对象
        print(isinstance(User(), User))  # True
        print(type(User()) == User)  # True
        print(isinstance(User(), People))  # True  isinstance考虑继承
        print(type(User()) == People)  # False type不考虑继承


pass

if __name__ == '__main__':
    unittest.main()
