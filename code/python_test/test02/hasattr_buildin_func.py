import unittest


class User:
    name: str
    age: int

    def say(self):
        pass


class MyTestCase(unittest.TestCase):
    def test_hasattr(self):
        user = User()
        user.name = 'iworkh'
        user.age = 120
        print(hasattr(user, 'name'))  # True 属性
        print(hasattr(user, 'sex'))  # False
        print(hasattr(user, 'say'))  # True 方法

if __name__ == '__main__':
    unittest.main()
