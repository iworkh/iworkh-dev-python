import unittest


class MyTestCase(unittest.TestCase):
    def test_any(self):
        print(any([]))  # False 空list为False
        print(any(()))  # False 空tuple为False
        print(any((0,0)))  # False 都0
        print(any(['','','']))  # False 都空
        print(any((1,2,3,4)))  #True
        print(any([0,0,2])) # True，有2
        print(any(['a','','c'])) # True， 有值

if __name__ == '__main__':
    unittest.main()
