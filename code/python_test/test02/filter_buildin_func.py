import unittest

def is_odd(n):
    return n % 2 == 1

class MyTestCase(unittest.TestCase):
    def test_filter(self):
        list_data = list(range(1, 10))
        # lambda
        result = filter(lambda item: item % 2 == 0, list_data) # 返回的是iter
        print(list(result)) # [2, 4, 6, 8]
        # func
        result2 = filter(is_odd, list_data)
        print(list(result2)) # [1, 3, 5, 7, 9]

if __name__ == '__main__':
    unittest.main()
