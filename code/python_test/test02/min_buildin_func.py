import unittest


class MyTestCase(unittest.TestCase):
    def test_min(self):
        print(min(10,2,3,4))  # 2
        print(min([10,2,3,4]))  # 2

    def test_max(self):
        print(max(10,2,3,4))  # 10
        print(max([10,2,3,4]))  # 10

if __name__ == '__main__':
    unittest.main()
