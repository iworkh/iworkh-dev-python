import unittest


class MyTestCase(unittest.TestCase):
    def test_next(self):
        list_data = [1, 2, 3, 4]
        it = iter(list_data)
        while (True):
            try:
                data = next(it)
                print(data)
            except StopIteration:
                break


if __name__ == '__main__':
    unittest.main()
