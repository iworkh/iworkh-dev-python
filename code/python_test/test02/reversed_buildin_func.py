import unittest


class MyTestCase(unittest.TestCase):
    def test_reversed(self):
        list_data = [1, 2, 3, 4]
        reversed_iter = reversed(list_data)  # 返回是iter
        print(list(reversed_iter)) # [4, 3, 2, 1]

        seqString = 'iworkh'
        print(list(reversed(seqString))) # ['h', 'k', 'r', 'o', 'w', 'i']

if __name__ == '__main__':
    unittest.main()
