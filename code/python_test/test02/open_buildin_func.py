import unittest


class MyTestCase(unittest.TestCase):
    def test_open(self):
        f = open('open_buildin_func.py')
        try:
            data = f.read()
            print(data)
        except Exception as e:
            print(e)
        finally:
            f.close()


if __name__ == '__main__':
    unittest.main()
