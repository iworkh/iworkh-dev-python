import unittest

global_a = 10
global_b = 'iworkh'


class MyTestCase(unittest.TestCase):
    def test_globals(self):
        # globals 函数返回一个全局变量的字典，包括所有导入的变量。
        all_globals = globals()
        print(all_globals['global_b'])


if __name__ == '__main__':
    unittest.main()
