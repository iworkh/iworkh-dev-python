import unittest

class MyTestCase(unittest.TestCase):
    def test_import(self):
        theme = 'dark'
        # theme = 'light'
        if theme == 'dark':
            __import__('theme.dark.import_dark_buildin_func')
        else:
            __import__('theme.light.import_light_buildin_func')

        print('done')

if __name__ == '__main__':
    unittest.main()
