import unittest


class User:
    _name: str

    def get_name(self):
        print('call method ... get_name')
        return self._name

    def set_name(self, value):
        print('call method ... set_name')
        self._name = value

    def del_name(self):
        print('call method ... del_name')
        del self._name

    name = property(get_name, set_name, del_name, '姓名')


class User2:
    _name: str

    @property
    def name(self):
        """
        姓名
        """
        print('call2 method ... get_name')
        return self._name

    @name.setter
    def name(self, value):
        print('call2 method ... set_name')
        self._name = value

    @name.deleter
    def name(self):
        print('call2 method ... del_name')
        del self._name


class MyTestCase(unittest.TestCase):
    def test_method(self):
        # 对象.方法
        user = User()
        user.set_name(value='iworkh')
        print(user.get_name())

    def test_property(self):
        # property
        user2 = User()
        user2.name = 'iworkh'
        print(user2.name)

    def test_anno(self):
        # property
        user2 = User2()
        user2.name = 'iworkh'
        print(user2.name)


if __name__ == '__main__':
    unittest.main()
