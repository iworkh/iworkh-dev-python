import unittest

class A(object):
    bar = 1
    def func1(self):
        print ('foo')

    @classmethod
    def func2(cls):
        print ('func2')
        print (cls.bar)
        cls().func1()   # 调用 func1 方法


class MyTestCase(unittest.TestCase):
    def test_classmethod(self):
        # 类调用
        A.func2()
        # 对象调用
        # a = A()
        # a.func2()

if __name__ == '__main__':
    unittest.main()
