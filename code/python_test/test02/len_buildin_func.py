import unittest


class MyTestCase(unittest.TestCase):
    def test_len(self):
        # str
        print(len('iworkh'))
        # list
        print(len([1, 2, 3]))
        # tuple
        print(len((1, 2, 3)))
        # dict
        dict = {'name': 'iworkh', 'age': 10}
        print(len(dict))


pass

if __name__ == '__main__':
    unittest.main()
