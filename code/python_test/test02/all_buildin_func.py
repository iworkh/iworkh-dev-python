import unittest


class MyTestCase(unittest.TestCase):
    def test_all(self):
        print(all([]))  # True 空list为true
        print(all(()))  # True 空tuple为true
        print(all((1,2,3,4)))  #True
        print(all([1,0,2])) # False，有0
        print(all(['a','','c'])) # False， 有空

if __name__ == '__main__':
    unittest.main()
