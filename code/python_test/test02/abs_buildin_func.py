import unittest


class MyTestCase(unittest.TestCase):
    def test_abs(self):
        a = 3.14
        b = -7.36
        c = complex(3, 4)  # 创建一个复数
        print(abs(a))  # 3.14
        print(abs(b))  # 7.36
        print(abs(c))  # 5.0


if __name__ == '__main__':
    unittest.main()
