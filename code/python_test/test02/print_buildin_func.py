import unittest


class MyTestCase(unittest.TestCase):
    def test_print(self):
        print("xxx")
        print("aaa""bbb")
        print("aaa","bbb")
        print("https://","www","iworkh","com",sep='.')


if __name__ == '__main__':
    unittest.main()
