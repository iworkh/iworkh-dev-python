import unittest


class MyTestCase(unittest.TestCase):
    def test_divmod(self):
        print(divmod(3, 2))  # (1, 1)
        print(divmod(3, -2))  # (-2, -1)
        print(divmod(-3, 2))  # (-2, 1)
        print(divmod(-3, -2))  # (1, -1)
        print(divmod(3, 1.2))  # (2.0, 0.6000000000000001)


if __name__ == '__main__':
    unittest.main()
