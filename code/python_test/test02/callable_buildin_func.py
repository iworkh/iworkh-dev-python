import unittest


#  函数
def fun_a():
    pass


# 没有实现__call__的类A
class A:
    def method(self):
        pass


# 实现__call__的类B
class B:
    no: str = 1
    _sex: str = '男'

    def __call__(self, *args, **kwargs):
        pass


class MyTestCase(unittest.TestCase):
    def test_call(self):
        # str、list、number
        print(callable(0))  # False
        print(callable('aaaa'))  # False
        print(callable([1, 2, 3]))  # False
        print(callable((1, 2, 3)))  # False
        # 函数
        print(callable(fun_a))  # True

        # 类属性
        print(callable(B.no))  # False 属性
        print(callable(B._sex))  # False 属性
        # 方法
        print(callable(A.method))  # True
        # 类
        print(callable(A))  # True
        print(callable(B))  # True
        # 对象
        a = A()
        print(callable(a))  # False 没实现`__call__`方法
        b = B()
        print(callable(b))  # True 实现了`__call__`方法

        print(callable(b.no))  # False 属性
        print(callable(b._sex))  # False 属性
        pass


if __name__ == '__main__':
    unittest.main()
