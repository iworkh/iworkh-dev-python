import unittest


class User:
    name: str
    age: int


class MyTestCase(unittest.TestCase):
    def test_getattr(self):
        user = User()
        user.name = 'iworkh'
        user.age = 20
        getattr(user, 'name')



if __name__ == '__main__':
    unittest.main()
