import unittest


class User:
    name: str
    age: int


class MyTestCase(unittest.TestCase):
    def test_repr(self):
        # str
        name = 'iwrokh'
        print(name)
        print(repr(name))

        # list
        list_data = [1, 2, 3, 4]
        print(list_data)
        print(repr(list_data))

        # dict
        dict = {'name': 'iworkh', 'age': 19}
        print(dict)
        print(repr(dict))

        # obj
        user = User
        user.name = 'iworkh'
        user.age = 11
        print(user)
        print(repr(user))


if __name__ == '__main__':
    unittest.main()
