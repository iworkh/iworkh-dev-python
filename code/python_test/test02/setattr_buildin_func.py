import unittest


class User:
    name: str
    age: int


class MyTestCase(unittest.TestCase):
    def test_setattr(self):
        # 对象
        user = User()
        user.name = 'iworkh'
        # 存在属性赋值
        setattr(user, 'age', 20)
        print(user.__dict__) # 20
        # 不存在属性赋值
        setattr(user, 'sex', '男')
        print(user.__dict__) # 男


if __name__ == '__main__':
    unittest.main()
