import unittest


class User:
    name: str
    password: str
    age: int


class MyTestCase(unittest.TestCase):
    def test_delattr(self):
        user = User()
        user.name = 'iworkh'
        user.password = 'iworkh123'
        user.age = 10
        print(user.__dict__)
        delattr(user, 'password') # 等价于 del user.password
        print(user.__dict__)


if __name__ == '__main__':
    unittest.main()
