import unittest


class MyTestCase(unittest.TestCase):
    def test_zip(self):
        a = [1, 2, 3]
        b = [4, 5, 6]
        c = [4, 5, 6, 7, 8]
        zip_tuple = zip(a, b)
        print(list(zip_tuple))
        print(list(zip(a, c)))  # 元素个数与最短的列表一致

        # 与 zip 相反，zip(*) 可理解为解压，返回二维矩阵式
        x, y = zip(*zip(a, c))  # 可以
        print(list(x))
        print(list(y))

        # m, n = zip(*zip_tuple)  # 报错 not enough values to unpack，上面有调用了list(zip_tuple)就会报错
        zip_tuple2 = zip(a, b)
        m, n = zip(*zip_tuple2) # 正确，没有对zip(a, b)的结果进行list转化
        print(list(m))
        print(list(n))

        pass


if __name__ == '__main__':
    unittest.main()
