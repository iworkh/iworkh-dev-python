import unittest


class MyTestCase(unittest.TestCase):
    def test_enumerate(self):
        list_data = ['a', 'b', 'c']
        e = enumerate(list_data, 0)
        for index, item in e:
            print("{} --- {}".format(index, item))

        pass


if __name__ == '__main__':
    unittest.main()
