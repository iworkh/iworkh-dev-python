import unittest


class User:
    name: str = 'iworkh'

    def say(self):
        pass

class MyTestCase(unittest.TestCase):
    def test_dir(self):
        # 空
        print(dir())
        # list
        print(dir([]))
        #  str
        print(dir('a'))
        # 对象
        print(dir(User)) #属性和方法都出来


if __name__ == '__main__':
    unittest.main()
