import unittest


class People:
    sex: str


class User(People):
    name: str
    age: int


class MyTestCase(unittest.TestCase):
    def test_issubclass(self):
        # 对象
        print(issubclass(User, People))  # True

pass

if __name__ == '__main__':
    unittest.main()
