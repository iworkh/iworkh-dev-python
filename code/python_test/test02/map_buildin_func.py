import unittest


class MyTestCase(unittest.TestCase):
    def test_map(self):
        list_data = [1, 2, 3, 4]
        result = map(lambda x: x * 2, list_data) # 返回iter
        print(list(result))

if __name__ == '__main__':
    unittest.main()
