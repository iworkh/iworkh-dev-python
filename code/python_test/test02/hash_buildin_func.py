import unittest


class User:
    name: str
    age: int


class MyTestCase(unittest.TestCase):
    def test_hash(self):
        print(hash(1))
        # 字符串
        print(hash("2"))
        # tuple
        print(hash((1, False, '3')))
        # dict要转str
        print(hash(str({'name': 'iworkh', 'age': 20})))
        # 对象
        user = User()
        user.name = 'iworkh'
        user.age = 120
        print(hash(user))


if __name__ == '__main__':
    unittest.main()
