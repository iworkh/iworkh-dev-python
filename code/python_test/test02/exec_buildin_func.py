import unittest


class MyTestCase(unittest.TestCase):
    def test_exec(self):
        # 普通字符串
        exec('print("Hello World")')

        # 有参数，使用dict传入
        source = """for i in range(num):
                    print ("iter time: %d" % i)
            """
        exec(source, {'num': 5})


if __name__ == '__main__':
    unittest.main()
