import unittest


class MyTestCase(unittest.TestCase):
    def test_memoryview(self):
        # str
        ba = bytearray('abcde', 'utf-8')
        mv = memoryview(ba)
        print(mv[1])  # 98
        print(mv[1:4])  # <memory at 0x000001D28801C948>
        print(mv[1:4].tobytes())  # b'bcd'
        print(mv[-1])  # 101

        print("*" * 50)

        # list
        list_data = [1, 2, 3, 4]
        ba_list = bytearray(list_data)
        mv_list = memoryview(ba_list)
        print(mv_list[0]) # 1

if __name__ == '__main__':
            unittest.main()
