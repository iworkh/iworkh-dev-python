import unittest


class User:
    name: str
    age: int


class MyTestCase(unittest.TestCase):
    def test_vars(self):
        # 对象
        user = User()
        user.name = 'iworkh'
        user.age = 11
        print(vars(user)) # {'name': 'iworkh', 'age': 11}



if __name__ == '__main__':
    unittest.main()
