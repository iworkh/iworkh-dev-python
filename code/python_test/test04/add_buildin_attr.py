import unittest


class Alphabet:
    data = []

    def __init__(self, value) -> None:
        self.data = value

    def __add__(self, other):
        print('call...__add__')
        return Alphabet(self.data + other.data)

    def __iadd__(self, other):
        print("call...__iadd__")
        self.data += other
        return self

class MyTestCase(unittest.TestCase):
    def test(self):
        list_data = ['a', 'c', 'd']
        alphabet = Alphabet(list_data)
        print(alphabet.data)
        print("*" * 50)
        # + 与 __add__
        alphabet_add = Alphabet(['m', 'n'])
        alphabet_sum = alphabet + alphabet_add  # 返回一个新的对象
        print(alphabet.data)  # 原来的没有变 ['a', 'c', 'd']
        print(alphabet_sum.data)  # ['a', 'c', 'd', 'm', 'n']
        print("*" * 50)
        # += 与 __iadd__
        alphabet += 'x'
        print(alphabet.data) # 在原对象上操作 ['a', 'c', 'd', 'x']


if __name__ == '__main__':
    unittest.main()
