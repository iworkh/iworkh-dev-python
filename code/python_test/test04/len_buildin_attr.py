import unittest


class Alphabet:
    data = []

    def __init__(self, value) -> None:
        self.data = value

    def __len__(self):
        return len(self.data)


class MyTestCase(unittest.TestCase):
    def test(self):
        list_data = ['a', 'c', 'd']
        alphabet = Alphabet(list_data)
        print(len(alphabet)) # 3


if __name__ == '__main__':
    unittest.main()
