import unittest


class Alphabet:
    data = []
    index = -1

    def __init__(self, value) -> None:
        self.data = value

    def __iter__(self):
        print("call...__iter__")
        return self

    def __next__(self):
        print("call...__next__")
        self.index += 1
        if self.index >= len(self.data):
            raise StopIteration()
        else:
            return self.data[self.index]


class MyTestCase(unittest.TestCase):
    def test(self):
        list_data = ['a', 'c', 'd']
        alphabet = Alphabet(list_data)
        print(alphabet.data)
        # for循环 和 __iter__ 以及 __next__配合
        for item in alphabet:
            print(item)


if __name__ == '__main__':
    unittest.main()
