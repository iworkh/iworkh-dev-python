import unittest


class Alphabet:
    data = []

    def __init__(self, value) -> None:
        self.data = value

    def __mul__(self, other):
        print("call...__mul__")
        return Alphabet(self.data * other)

    def __rmul__(self, other):
        print("call...__rmul__")
        return Alphabet(other * self.data)

    def __imul__(self, other):
        print("call...__imul__")
        return self.data * other


class MyTestCase(unittest.TestCase):
    def test(self):
        list_data = ['a', 'c', 'd']
        alphabet = Alphabet(list_data)
        print(alphabet.data)
        # __mul__
        alphabet_3 = alphabet * 3
        print(alphabet.data)
        print(alphabet_3.data)  # ['a', 'c', 'd', 'a', 'c', 'd', 'a', 'c', 'd']
        print("*" * 50)
        # __rmul__
        alphabet_r3 = 3 * alphabet
        print(alphabet.data)
        print(alphabet_r3.data)  # ['a', 'c', 'd', 'a', 'c', 'd', 'a', 'c', 'd']
        print("*" * 50)
        # __imul__
        alphabet *= 3  # 改变了原对象
        print(alphabet)  # ['a', 'c', 'd', 'a', 'c', 'd', 'a', 'c', 'd']


if __name__ == '__main__':
    unittest.main()
