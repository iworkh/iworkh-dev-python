import unittest


class Alphabet:
    data = []

    def __init__(self, value) -> None:
        self.data = value

    def __getitem__(self, index):
        print('call...__getitem__')
        return self.data[index]

    def __setitem__(self, key, value):
        print('call...__setitem__')
        self.data[key] = value

    def __delitem__(self, key):
        print("call...__delitem__")
        delattr(self, key)


class MyTestCase(unittest.TestCase):
    def test(self):
        list_data = ['a', 'c', 'd']
        alphabet = Alphabet(list_data)
        print(alphabet.data)
        print("*" * 30) # ['a', 'c', 'd']
        # 取值
        print(alphabet[1])  # c 取值 会触发__getitem__
        # 设置值
        alphabet[1] = 'b'  # 设值 会触发__setitem__
        print(alphabet.data) # ['a', 'b', 'd']
        # 删除
        del alphabet['data']  # 删除 等价于 delattr(alphabet, 'data')  会触发__delitem__
        print(alphabet.data) # []


if __name__ == '__main__':
    unittest.main()
