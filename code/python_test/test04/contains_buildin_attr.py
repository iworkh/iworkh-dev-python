import unittest


class Alphabet:
    data = []

    def __init__(self, value) -> None:
        self.data = value

    def __contains__(self, item):
        print("call...__contains__")
        return item in self.data


class MyTestCase(unittest.TestCase):
    def test(self):
        list_data = ['a', 'c', 'd']
        alphabet = Alphabet(list_data)
        print(alphabet.data)
        print('a' in alphabet) # True
        print('x' in alphabet) # False


if __name__ == '__main__':
    unittest.main()
