import unittest


class People:
    name: str = 'iworkh'
    age: int = 20
    admin: bool = True

    def __init__(self):
        self.admin = False

class MyTestCase(unittest.TestCase):
    def test(self):
        peo = People()
        print(peo.__dict__)  # {'admin': False}，只有init和塞值的才会显示
        print(vars(peo))  # {'admin': False}
        # 塞值后
        peo.name = '沐雨云楼'
        print(peo.__dict__)  # 对象的__dict__ , {'admin': False, 'name': '沐雨云楼'}
        print(vars(peo))  # {'admin': False, 'name': '沐雨云楼'}
        print(People.__dict__)  # 类的__dict__, 很多，还包括__和自己定义的


if __name__ == '__main__':
    unittest.main()
