import unittest


class People:
    name: str = 'iworkh'
    age: int = 20

    def __str__(self) -> str:
        print("call...__str__")
        return "{{'name': {}, 'age': {}}}".format(self.name, self.age)


class MyTestCase(unittest.TestCase):
    def test(self):
        people = People()
        print(people) # {'name': iworkh, 'age': 20}
        print(str(people)) # {'name': iworkh, 'age': 20}


if __name__ == '__main__':
    unittest.main()
