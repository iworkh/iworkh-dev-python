import unittest


class People:
    name: str = 'iworkh'
    age: int = 20

    def __str__(self) -> str:
        print("call...__str__")
        return "str --> {{'name': {}, 'age': {}}}".format(self.name, self.age)

    def __repr__(self):
        print("call...__repr__")
        return "repr --> {{'name': {}, 'age': {}}}".format(self.name, self.age)


class MyTestCase(unittest.TestCase):
    def test(self):
        people = People()
        print(people)
        print(str(people))


if __name__ == '__main__':
    unittest.main()
