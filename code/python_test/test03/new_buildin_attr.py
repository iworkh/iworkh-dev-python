import unittest


class People:
    name: str = 'iworkh'

    def __new__(cls, *args, **kwargs):
        print('call...__new__')
        obj = object.__new__(cls)
        return obj

    def __init__(self, name):
        print('call...__init__')
        self.name = name


class MyTestCase(unittest.TestCase):

    def test(self):
        people = People('沐雨云楼')  # 创建对象先调用__new__，初始化时调用__init__
        print(people.name)


if __name__ == '__main__':
    unittest.main()
