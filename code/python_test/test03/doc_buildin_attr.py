import unittest


class People:
    """
    人
    """
    name: str = 'iworkh'
    age: int = 20

    def say(self):
        """
        say method
        """
        print('say')


class MyTestCase(unittest.TestCase):
    def test(self):
        people = People()
        print(People.__doc__)  # 类： 人
        print(people.__doc__)  # 对象：人
        print(People.say.__doc__)  # 类：say method
        print(people.say.__doc__)  # 对象：say method
        print(help(People.say))  # 类：say method


if __name__ == '__main__':
    unittest.main()
