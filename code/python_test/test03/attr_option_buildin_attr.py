import unittest


class People:
    name: str = 'iworkh'
    password = 'iworkh123'

    def __getattr__(self, item):
        print('call...__getattr__')
        return 'no attribute'

    def __delattr__(self, item):
        print('call...__delattr__')
        object.__delattr__(self, item)

    def __setattr__(self, key, value):
        print('call...__setattr__')
        object.__setattr__(self, key, value)


class MyTestCase(unittest.TestCase):
    def test(self):
        peo = People()
        setattr(peo, 'name', '沐雨云楼')  # 塞值
        # 取值
        print(peo.name)  # 存在， 不会触发__getattr__
        print(peo.sex)  # 不存在，调用 __getattr__
        print(getattr(peo, 'name'))  # 存在， getattr方式 不会触发__getattr__
        print(getattr(peo, 'sex'))  # 不存在，getattr方式 调用 __getattr__
        # 删除
        peo.password = "test_password"  # 塞值
        del peo.password
        delattr(peo, 'name')


if __name__ == '__main__':
    unittest.main()
