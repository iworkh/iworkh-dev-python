import unittest


class People:
    name: str = 'iworkh'
    age: int = 20

    def __call__(self, **kwargs):
        print('call...__call__')
        self.name = kwargs['name']
        self.age = kwargs['age']


class MyTestCase(unittest.TestCase):
    def test(self):
        peo1 = People()
        print(peo1.name, peo1.age, sep="......") # iworkh......20
        peo1(name='沐雨云楼', age=22) # 通过对象()方式就能修改值
        print(peo1.name, peo1.age, sep="......") # 沐雨云楼......22


if __name__ == '__main__':
    unittest.main()
