import unittest

class People:
    def __init__(self, name, age):
        self.name = name
        self.age = age

    def __le__(self, other):
        print('call ... le')
        return self.age <= other.age


class MyTestCase(unittest.TestCase):
    def test(self):
        people_san = People('三哥', 30)
        people_three = People('张三', 30)
        people_four = People('李四', 40)
        people_five = People('王五', 50)
        print(people_three <= people_four) # True
        print(people_five >= people_three) # True
        print(people_san >= people_three) # True
        print(people_san <= people_three) # True
        print(people_three >= people_four) # False



if __name__ == '__main__':
    unittest.main()
