import unittest


class People:
    name: str = 'iworkh'
    password = 'iworkh123'
    age: int = 20

    def __getattribute__(self, item):
        print("call .... __getattribute__")
        if item == 'password':
            return '保密'
        else:
            return object.__getattribute__(self, item)


class MyTestCase(unittest.TestCase):
    def test(self):
        peo = People()
        print(peo.name)  # iworkh
        print(peo.password)  # 保密


if __name__ == '__main__':
    unittest.main()
