import unittest


class People:
    name: str = 'iworkh'


class Student(People):
    """
    学生类
    """
    no: int = 1
    _sex: str = '男'

    @property
    def sex(self):
        return self._sex

    @classmethod
    def cm_sex(cls):
        return cls._sex


class MyTestCase(unittest.TestCase):
    def test_attr(self):
        print(dir([1,2,3]))

    def test_base(self):
        print(People.__base__)  # 类
        print(Student.__base__)  # 类

    def test_annotations(self):
        print(Student.__annotations__)  # 类
        print(Student().__annotations__)  # 对象
        print(People().__annotations__)  # 对象

    def test_call(self):
        st = Student()  # 类
        print(st.name)
        st_call = Student.__call__() # 类.__call__
        print(st_call.name)

    def test_class(self):
        print(Student().__class__)  # 对象


if __name__ == '__main__':
    unittest.main()
